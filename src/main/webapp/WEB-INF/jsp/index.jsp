<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Daleleo</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="/">Ver Bodegas</a></li>
      <li><a href="new">Crear una bodega</a></li>
    </ul>
  </div>
</nav>
  
<div class="container">
	<c:choose>
	  <c:when test = "${mode == 'BODEGA_VIEW'}">  
  <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Bodega</th>
        <th>Tama&ntilde;o</th>
                <th>Editar</th>
                 <th>Eliminar</th>
      </tr>
    </thead>
    <tbody>
  
    <c:forEach var="bodega" items="${bodegas}">
      <tr>
        <td>${bodega.bodega_id}</td>
        <td>${bodega.name}</td>
         <td>${bodega.size}</td>
         <td><a href ="/updateBodega?id=${bodega.bodega_id}"> <div class="glyphicon glyphicon-pencil"></div> </a></td>
     	<td><a href ="/delete?id=${bodega.bodega_id}"> <div class="glyphicon glyphicon-trash"></div> </a></td>
      </tr>
    </c:forEach>
      </c:when>
     <c:when test = "${mode == 'BODEGA_EDIT' || mode == 'NEW_BODEGA'}">  
     
      	<form action="save" method="post">
      	
      	    <input type="hidden" class="form-control" value="${bodega.bodega_id}" name = "bodega_id" id="bodega_id">
					  <div class="form-group">
					    <label for="name">Bodega:</label>
					    <input type="text" class="form-control" value = "${bodega.name}" id="name" name = "name">
					  </div>
					  <div class="form-group">
					    <label for="size">Tama&ntilde;o:</label>
					    <input type="text" class="form-control" value="${bodega.size}" id="size" name = "size">
					  </div>
					  
					  <button type="submit" class="btn btn-default">Submit</button>
		</form> 
				     
      </c:when>
    </c:choose>
    
    </tbody>
  </table>
</div>

</body>
</html>
