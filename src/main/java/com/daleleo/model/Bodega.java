package com.daleleo.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;



@Entity(name = "bodega")
public class Bodega {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long bodega_id;
	@Column(name = "name")
	private String name;
	@Column(name = "size")
	private int size;
	
	
	public long getBodega_id() {
		return bodega_id;
	}
	public void setBodega_id(long bodega_id) {
		this.bodega_id = bodega_id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	

}
