package com.daleleo.dao;

import org.springframework.stereotype.Repository;
import com.daleleo.model.Bodega;
import org.springframework.data.repository.CrudRepository;
@Repository
public interface BodegaRepository extends CrudRepository<Bodega,Long> {
	

}
