package com.daleleo.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.daleleo.model.Bodega;
import com.daleleo.services.BodegaService;

@RestController
public class MainRestController {
	
	@Autowired
	private BodegaService bodegaservice;
	

	public String  hello() {
		return "hello World";
	}
	public Collection<Bodega> getAllBodegas(){
		return bodegaservice.findAllBodegas();
	}
	
	
}
