package com.daleleo.controllers;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.daleleo.model.Bodega;
import com.daleleo.services.BodegaService;

@Controller
public class BodegaController {
	@Autowired
	private BodegaService bodegaservice;
	@GetMapping("/")
	public  String init(HttpServletRequest req) {
	req.setAttribute("bodegas", bodegaservice.findAllBodegas());
	req.setAttribute("mode", "BODEGA_VIEW");
	return "index";
	}
	
	
	@GetMapping("/updateBodega")
	public  String init(@RequestParam long id,HttpServletRequest req) {
		req.setAttribute("bodega", bodegaservice.findOne(id));
		req.setAttribute("mode", "BODEGA_EDIT");
		return "index";
		}
	
	@PostMapping("/save")
	public void save(@ModelAttribute Bodega bodega,BindingResult bindingx,HttpServletRequest req,HttpServletResponse resp) throws IOException{ 
		
		System.out.print(bodega);
		bodegaservice.save(bodega);
		req.setAttribute("bodegas", bodegaservice.findAllBodegas());
		req.setAttribute("mode", "BODEGA_VIEW");	
		resp.sendRedirect("/");
		
	
}
	@GetMapping("/new")
	public String newbodega(HttpServletRequest req) {
		req.setAttribute("mode","NEW_BODEGA");
		return "index";	
	}
	@GetMapping("/delete")
	public void deletebodega(@RequestParam long id,HttpServletRequest req,HttpServletResponse resp) throws IOException {
		bodegaservice.delete(id);
		req.setAttribute("bodegas", bodegaservice.findAllBodegas());
		req.setAttribute("mode", "BODEGA_VIEW");	
		resp.sendRedirect("/");	
	}
	
}