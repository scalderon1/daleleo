package com.daleleo.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.daleleo.model.Bodega;


import com.daleleo.dao.BodegaRepository;

@Service
public class BodegaService {
	@Autowired
	private BodegaRepository bodegaRepository;
	public Collection<Bodega> findAllBodegas(){
		List<Bodega> bodegas = new ArrayList<Bodega>();
		for (Bodega bodega: bodegaRepository.findAll()) {
			bodegas.add(bodega);
		}
		return bodegas;	
	}
	
	public void delete(long id) {
		bodegaRepository.deleteById(id);
	}
	
	public Bodega findOne(long id) {
		return bodegaRepository.findById(id).get();
	}
	
	public void save(Bodega bodega) {
		bodegaRepository.save(bodega);
	
	}
	
}
